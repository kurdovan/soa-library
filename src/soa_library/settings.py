#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Django settings for SOA library."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
# from __future__ import unicode_literals #FIXME: Still broken as of Django 1.3.

from lck.django import current_dir_support
execfile(current_dir_support)

from lck.django import namespace_package_support
execfile(namespace_package_support)

#
# common stuff for each install
#

ADMINS = (
    ('Lukasz Langa', 'lukasz@langa.pl'),
)
MANAGERS = ADMINS
DEFAULT_FROM_EMAIL = 'lukasz@langa.pl'
SERVER_EMAIL = DEFAULT_FROM_EMAIL
TIME_ZONE = 'Europe/Warsaw'
LANGUAGE_CODE = 'pl-pl'
SITE_ID = 1
USE_I18N = True
USE_L10N = True
MEDIA_ROOT = CURRENT_DIR + 'uploads'
MEDIA_URL = '/uploads/'
STATIC_ROOT = CURRENT_DIR + 'static'
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    CURRENT_DIR + 'media',
)
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'lck.django.staticfiles.LegacyAppDirectoriesFinder',
)
ADMIN_MEDIA_PREFIX = STATIC_URL + 'admin/'
FILE_UPLOAD_TEMP_DIR = CURRENT_DIR + 'uploads-part'
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)
MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    #'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    #'lck.django.common.middleware.ActivityMiddleware',
    'lck.django.common.middleware.AdminForceLanguageCodeMiddleware',
)
ROOT_URLCONF = 'soa_library.urls'
TEMPLATE_DIRS = (CURRENT_DIR + "templates",)
LOCALE_PATHS = (CURRENT_DIR + "locale",)
INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    #'django.contrib.comments',
    #'django.contrib.flatpages',
    'django_evolution',
    'djcelery',
    'lck.django.common',
    'gunicorn',
    'tastypie',
    'soa_library.books',
    'soa_library.rental',
)
TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.messages.context_processors.messages',
)
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'file': {
            'level': 'INFO',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'when': 'midnight',
            'filename': None, # to be configured in settings-local.py
            'formatter': 'verbose',
            'interval': 1,
        }
    },
    'formatters': {
        'verbose': {
            'datefmt': '%H:%M:%S',
            'format': '%(asctime)08s,%(msecs)03d %(levelname)-7s [%(processName)s %(process)d] %(module)s - %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'soa_library': {
            'handlers': ['file'],
            'propagate': True,
            'level': 'INFO',
        },
        'django_auth_ldap': {
            'handlers': ['file'],
            'propagate': True,
            'level': 'DEBUG',
        }
    },
}
FORCE_SCRIPT_NAME = ''
LOGIN_REDIRECT_URL = '/browse/'
LOGIN_URL = '/login/'
LOGOUT_URL = '/logout/'
# django.contrib.messages settings
MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'

# Celery
from multiprocessing import cpu_count
CELERYD_CONCURRENCY = min(4 * cpu_count(), 32)
CELERY_SEND_TASK_ERROR_EMAILS = True
BROKER_POOL_LIMIT = 4 * CELERYD_CONCURRENCY
CELERY_RESULT_BACKEND = "disabled"
CELERY_DISABLE_RATE_LIMITS = True

#
# stuff that should be customized in settings_local.py
#
SECRET_KEY = 'p3e#7+q4jb12=-yun5w!am(^q=oqot--i-r=y@=-8$*v5yexkn'
DEBUG = True
TEMPLATE_DEBUG = DEBUG
DUMMY_SEND_MAIL = DEBUG
SEND_BROKEN_LINK_EMAILS = DEBUG
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': CURRENT_DIR + 'development.db',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
        'OPTIONS': {'timeout': 30},
    },
    'rental': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': CURRENT_DIR + 'rental.db',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
        'OPTIONS': {'timeout': 30},
    },
}
LOGGING['handlers']['file']['filename'] = CURRENT_DIR + 'runtime.log'
BROKER_URL = "redis://localhost:6379/2"
DEFAULT_SAVE_PRIORITY = 0
API_LIMIT_PER_PAGE = 50
BOOK_REPOSITORY = None 

#
# programmatic stuff that needs to be in
#

import djcelery
djcelery.setup_loader()

from lck.django import profile_support
execfile(profile_support)
