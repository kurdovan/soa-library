from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
from tastypie.api import Api

from soa_library.books.api import AuthorResource, BookResource

admin.autodiscover()

v01_api = Api(api_name='v0.1')
v01_api.register(AuthorResource())
v01_api.register(BookResource())

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'soa_library.views.home', name='home'),
    # url(r'^soa_library/', include('soa_library.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(v01_api.urls)),
)
